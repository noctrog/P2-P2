// -*- mode: c++ -*-

// Compilar con --std=c++11 o superior

#ifndef TREE
#define TREE

#include <iostream>
#include <string>
#include <queue>

///////////////////////////////////////////
// TYPEDEF's y DECLARACIONES ADELANTADAS //
///////////////////////////////////////////
typedef  char Element; // Hasta que veamos genericidad usaremos estos
                       // typedef.
typedef  int Position;

struct   Node;
typedef  Node* NodePtr;

struct   Tree;
typedef  Tree* TreePtr;

//////////
// TREE //
//////////
struct Tree {
  NodePtr _root;
};


TreePtr      treeCreate     ();
void         treeDestroy    (TreePtr t);
NodePtr&     treeRoot       (TreePtr t);
bool         treeEmpty      (TreePtr t);
bool         treeInsert     (TreePtr t, Element x);
bool         treeRemove     (TreePtr t, Element x);
void         treeMakeNull   (TreePtr t);
uint         treeSize       (TreePtr t); // Número de nodos en el árbol
uint         treeHeight     (TreePtr t); // Altura del árbol
uint         treeLeafNodes  (TreePtr t); // Número de nodos hoja
NodePtr      treeSearch     (TreePtr t, Element x); // Busca x en el árbol
NodePtr      treeParent     (TreePtr t, Element x); // Devuelve el padre del nodo con clave=x
NodePtr      treeParent     (TreePtr t, NodePtr p); // Devuelve el padre del nodo p
NodePtr      treeMaximum    (TreePtr t);
std::string  treePreOrder   (TreePtr t, char c);
std::string  treePostOrder  (TreePtr t, char c);
std::string  treeInOrder    (TreePtr t, char c);
std::string  treeByLevels   (TreePtr t, char c);


/********/
/* NODE */
/********/
struct Node {
  Element _key;
  Tree lefts, rights;
};

NodePtr      nodeCreate        (Element e);
TreePtr      nodeLeftSibling   (NodePtr p);
TreePtr      nodeRightSibling  (NodePtr p);
Element&     nodeKey           (NodePtr p);
bool         nodeIsLeaf        (NodePtr p);
static std::string  nodeToString      (NodePtr p) {
  std::string s;
  return s + p->_key;
}

#endif
