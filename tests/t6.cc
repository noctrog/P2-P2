
/////////////////////
// search & parent //
/////////////////////

#include "tree.h"
#include <iostream>

int main(int argc, char *argv[])
{
  TreePtr t = treeCreate ();

  treeInsert (t, 'g');
  treeInsert (t, 'e');
  treeInsert (t, 'f');
  treeInsert (t, 'b');
  treeInsert (t, 'h');

  NodePtr n1 = treeSearch (t, 'b');
  NodePtr n2 = treeParent (t, n1);
  auto r = nodeKey(n2) == 'e';
  
  treeDestroy (t);
  
  return r ? 0 : 1;
}
