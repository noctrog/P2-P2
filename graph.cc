#include "graph.h"
#include <set>

GraphPtr    graphCreate      (uint nv, bool directed)
{
  GraphPtr g = new Graph;
  g->nv = nv;
  g->ne = 0;
  g->d = directed;

  g->v = new weightRow_t[nv];
  for (uint i = 0; i < nv; ++i)
  {
    g->v[i] = new weight_t[nv]();

    // Inicializar a infinito (sin aristas), y las diagonales a 0
    for (uint j = 0; j < nv; ++j)
    {
      if (i != j)
        g->v[i][j] = NOEDGE;
      else
        g->v[i][j] = NOWEIGHT;
    }
  }

  return g;
}
void        graphDestroy     (GraphPtr g)
{
  // Liberar memoria de los vertices
  if (g)
  {
    graphMakeNull(g);

    // Liberar el propio grafo en si
    delete g;
    g = nullptr;
  }
}

// void        graphDestroy     (GraphPtr& g)
// {
//   // Liberar memoria de los vertices
//   if (g)
//   {
//     graphMakeNull(g);
//
//     // Liberar el propio grafo en si
//     delete g;
//     g = nullptr;
//   }
// }

void        graphMakeNull    (GraphPtr g)
{
  if (g && g->nv > 0)
  {
    for (uint i = 0; i < g->nv; ++i)
    {
      delete[] g->v[i];
      g->v[i] = nullptr;
    }
    delete[] g->v;
    g->v = nullptr;
  }

  g->nv = 0;
  g->ne = 0;
}
int         graphVertexSize  (GraphPtr g)
{
  // Si existe el grafo
  if (g)
    return g->nv;
  // Si no existe el grafo
  else
    return 0;
}
int         graphEdgeSize    (GraphPtr g)
{
  if (g)
    return g->ne;
  else
    return 0;
}
bool        graphInsert      (GraphPtr g, size_t o, size_t e,
                              weight_t w)
{
  bool success = true;

  // Si existe el grafo, o el origen y destino no estan en los limites
  if (!(g && g->v && o >= 0 && e >= 0 && o < g->nv && e < g->nv))
  {
    success = false;
  }
  else
  {
      if (g->v[o][e] == NOEDGE)
      {
        g->ne = g->ne + 1;
      }

      // Introducir arista
      g->v[o][e] = w;

      // Si es un grafo no dirigido, la matriz es simetrica
      if (not g->d)
      {
        g->v[e][o] = w;
      }
  }

  return success;
}

std::vector<weight_t>
dijkstra (GraphPtr g, size_t i)
{
  // Comprueba que existe el nodo o existe el grafo
  if (i < 0 || i >= g->nv || g == nullptr || g->v == nullptr || g->nv == 0)
  {
    return {};
  }
  else
  {
    // Declara las variables a usar
    struct dtag
    {
      bool bLocked;
      weight_t weight;
      size_t from;
    };

    // Guarda el progreso del algoritmo
    std::vector<dtag> columns;

    // Inicializa las variables
    for (size_t j = 0; j < g->nv; ++j)
    {
      // Al comienzo todos los nodos estan a distancia infinita de i
      columns.push_back({false, NOEDGE, 0});
    }

    /* --- Comienzo del algoritmo ---*/

    // Fijar el primer punto
    columns.at(i).bLocked = 0;
    columns.at(i).weight = NOWEIGHT;
    columns.at(i).from = i;

    // Almacena el nodo actual
    size_t currentNode = i;

    // Algoritmo de dijkstra
    for (size_t j = 1; j < g->nv; ++j)
    {
      // Recorrer columnas, actualizandolas con las nuevas opciones
      for (size_t x = 0; x < g->nv; x++) {
        // Se lockea al nodo actual
        columns.at(currentNode).bLocked = true;

        // Se leen los valores de la matriz de adyacencia y se cogen los que
        // son menores a los actuales
        if (!columns.at(x).bLocked && (g->v[currentNode][x] != NOEDGE) &&
           (columns.at(currentNode).weight + g->v[currentNode][x]) < columns.at(x).weight)
        {
          columns.at(x).from = currentNode;
          columns.at(x).weight = columns.at(currentNode).weight + g->v[currentNode][x];
        }
      }
      // Se coge el camino de menos peso
      weight_t min = INFINITY;
      for (size_t y = 0; y < g->nv; ++y)
      {
        min = (min > columns.at(y).weight && not columns.at(y).bLocked) ? columns.at(y).weight : min;
      }
      // Si no hay mas caminos, terminar algoritmo
      if (min == INFINITY)
      {
        break;
      }
      // Se selecciona el nuevo nodo
      for (size_t y = 0; y < g->nv; ++y)
      {
        if (columns.at(y).weight == min)
        {
          currentNode = y;
          break;
        }
      }

    }

    // Preparar solucion
    std::vector<weight_t> res;

    for (size_t x = 0; x < g->nv; ++x)
    {
      res.push_back(columns.at(x).weight);
    }

    return res;
  }
}

std::vector<Edge>
prim (GraphPtr g, size_t i)
{
  // Comprueba que existe el nodo o existe el grafo
  if (i < 0 || i >= g->nv || g == nullptr || g->v == nullptr || g->nv == 0)
  {
    return {};
  }
  else
  {
    std::set<uint> U;
    std::vector<Edge> T;

    U.insert(i);

    while (U.size() != g->nv)
    {
      //(u, v) La arista de coste minimo tal que u esta en U y v en V-U
      uint u = 0, v = 0;
      weight_t minEdge = INFINITY;
      for (auto i : U)
      {
        for (uint j = 0; j < g->nv; ++j)
        {
          if (U.find(j) == U.end())
          {
            if (g->v[i][j] < minEdge)
            {
              u = i;
              v = j;
              minEdge = g->v[i][j];
            }
          }
        }

      }
      // Parar la ejecucion del algoritmo en caso de que no sea conexo y se hayan
      // alcanzado todos los vertices
      if (minEdge == INFINITY)
      {
        break;
      }

      T.push_back({u, v});
      U.insert(v);
    }
    return T;
  }
}
