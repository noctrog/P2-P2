  #include "tree.h"

/* --- Funciones auxiliares --- */

// Devuelve nullptr en caso de tener 2 o ningun hijo, devuelve al hijo en caso contrario
NodePtr nodeUniqueSibling(const NodePtr& p)
{
  NodePtr pleft = treeRoot(nodeLeftSibling(p));
  NodePtr pright = treeRoot(nodeRightSibling(p));

  if (pleft == nullptr && pright)
  {
    return pright;
  }
  else if (pright == nullptr && pleft)
  {
    return pleft;
  }
  else
  {
    return nullptr;
  }

  // if ((treeEmpty(nodeLeftSibling(p)) && not treeEmpty(nodeRightSibling(p))) ||
  //      (treeEmpty(nodeRightSibling(p)) && not treeEmpty(nodeLeftSibling(p))))
  // {
  //   return true;
  // }
  // else
  // {
  //   return false;
  // }
}

// Devuelve verdadero si un hijo de la raiz contiene a x
bool esPadre(const TreePtr t, const Element x)
{
  if (treeRoot(nodeLeftSibling(treeRoot(t))))
  {
    if (nodeKey(treeRoot(nodeLeftSibling(treeRoot(t)))) == x)
    {
      return true;
    }
  }
  if (treeRoot(nodeRightSibling(treeRoot(t))))
  {
    if (nodeKey(treeRoot(nodeRightSibling(treeRoot(t)))) == x)
    {
      return true;
    }
  }

  return false;
}

// Cosas nazis
void levels_recur(TreePtr t, std::queue<Element>& q) {
  // Caso base: no hacer nada

  // Caso recursivo
  if (not nodeIsLeaf(treeRoot(t)))
  {
    // Meter el valor de los dos hijos en la cola, si existen
    if (not treeEmpty(nodeLeftSibling(treeRoot(t))))
    {
      q.push(nodeKey(treeRoot(nodeLeftSibling(treeRoot(t)))));
    }
    if (not treeEmpty(nodeRightSibling(treeRoot(t))))
    {
      q.push(nodeKey(treeRoot(nodeRightSibling(treeRoot(t)))));
    }

    // Ejecutar recursivamente en cada hijo, si existen
    if (treeRoot(nodeLeftSibling(treeRoot(t))))
    {
      levels_recur(nodeLeftSibling(treeRoot(t)), q);
    }
    if (treeRoot(nodeRightSibling(treeRoot(t))))
    {
      levels_recur(nodeRightSibling(treeRoot(t)), q);
    }
  }
}

/* --- Funciones --- */

TreePtr      treeCreate     ()
{
  TreePtr t = new Tree;
  treeRoot(t) = nullptr;

  return t;
}

void         treeDestroy    (TreePtr t)
{
  if (treeRoot(t))
    treeMakeNull(t);
  delete t;
}

NodePtr&     treeRoot       (TreePtr t)
{
  return t->_root;
}
bool         treeEmpty      (TreePtr t)
{
  return (treeRoot(t) == nullptr) ? true : false;
}
bool         treeInsert     (TreePtr t, Element x)
{
  bool success = true;

  // Caso base: árbol vacio
  if (treeEmpty(t))
  {
    treeRoot(t) = nodeCreate(x);
  }
  // Caso base: insertar directamente en un nodo
  // Ocurre si el element a insertar es menor que treeRoot(t) y nodeLeftSibling(t) no existe
  // o si el elemento a insertar es mayor que treeRoot(t) y nodeRightSibling(t) no existe
  else if (nodeKey(treeRoot(t)) > x and treeEmpty(nodeLeftSibling(treeRoot(t))))
  {
    // Insertar a la izquierda
    treeRoot(nodeLeftSibling(treeRoot(t))) = nodeCreate(x);
  }
  else if ( nodeKey(treeRoot(t)) < x and treeEmpty(nodeRightSibling(treeRoot(t))))
  {
    // Insertar a a la derecha
    treeRoot(nodeRightSibling(treeRoot(t))) = nodeCreate(x);
  }
  // Caso recursivo
  else
  {
    // Si el nodo es igual a la raiz
    if (x == nodeKey(treeRoot(t)))
    {
      success = false;
    }
    // Si el nodo es menor que la raíz
    // Ya se ha comprobado que existe en el caso base
    else if (x < nodeKey(treeRoot(t)))
    {
      success = treeInsert(nodeLeftSibling(treeRoot(t)), x);
    }
    // Si el nodo es mayor que la raíz
    // Ya se ha comprobado que existe el camino derecho en el caso base
    else
    {
      success = treeInsert(nodeRightSibling(treeRoot(t)), x);
    }
  }

  return success;
}
bool         treeRemove     (TreePtr t, Element x)
{
  // obtener el puntero al elemento
  NodePtr p = treeSearch(t, x);

  // Si no se ha podido encontrar
  if (not p)
  {
    return false;
  }
  else
  {
    // Si es una hoja, eliminar directamente
    if (nodeIsLeaf(p))
    {
      treeMakeNull(t);
    }
    // Si solo tiene un hijo, hacer padre a su abuelo
    else if (nodeUniqueSibling(p))
    {
      // Obtener el nodo hijo
      NodePtr hijo = nodeUniqueSibling(p);
      // Obtener el nodo abuelo
      NodePtr abuelo = treeParent(t, p);

      // Si no tiene abuelo, es la raiz del arbol
      if (abuelo == nullptr && nodeKey(treeRoot(t)) == x)
      {
        NodePtr aux = treeRoot(t);
        if (not treeEmpty(nodeLeftSibling(treeRoot(t))))
        {
          treeRoot(t) = treeRoot(nodeLeftSibling(treeRoot(t)));
        }
        else
        {
          treeRoot(t) = treeRoot(nodeRightSibling(treeRoot(t)));
        }
        delete aux;
        aux = nullptr;
      }
      else
      {
        // Si p está a la izquierda de su padre
        if (nodeKey(p) < nodeKey(abuelo))
        {
          // Insertar hijo a su izquierda
          treeRoot(nodeLeftSibling(abuelo)) = hijo;
          // abuelo->lefts._root = hijo;
        }
        else
        {
          // p estará a la drecha de abuelo
          treeRoot(nodeRightSibling(abuelo)) = hijo;
          // abuelo->rights._root = hijo;
        }
      }

      // Elminar nodo p
      if (p)
        delete p;
    }
    // Si tiene dos hijos
    else
    {
      // Seleccionar arbol de la derecha
      TreePtr it = nodeRightSibling(p);

      // Iterar hasta encontrar el valor minimo
      for (; not treeEmpty(nodeLeftSibling(treeRoot(it))); it = nodeLeftSibling(treeRoot(it)));

      // Copiar el nodo de valor minimo del arbol derecho al nodo a eliminar
      nodeKey(p) = nodeKey(treeRoot(it));

      // Eliminar el nodo it
      treeMakeNull(it);
    }

    return true;
  }
}
void         treeMakeNull   (TreePtr t)
{
  // Caso base
  if (nodeIsLeaf(treeRoot(t)))
  {
    delete treeRoot(t);
    treeRoot(t) = nullptr;
  }
  // Recusivo
  else
  {
    // Si existe un hijo a la izquierda, eliminarlo
    if (not treeEmpty(nodeLeftSibling(treeRoot(t))))
    {
      treeMakeNull(nodeLeftSibling(treeRoot(t)));
    }
    // Si existe un hijo a la izquierda, eliminarlo
    if (not treeEmpty(nodeRightSibling(treeRoot(t))))
    {
      treeMakeNull(nodeRightSibling(treeRoot(t)));
    }

    delete treeRoot(t);
    treeRoot(t) = nullptr;
  }
}
uint         treeSize       (TreePtr t)
{
  uint size = 0;

  // Caso base: arbol vacio (no hacer nada)
  if (not treeEmpty(t))
  {
    // Caso base: es una hoja
    if (nodeIsLeaf(treeRoot(t)))
    {
      size++;
    }
    // Caso recursivo
    else
    {
      size++;
      size += treeSize(nodeLeftSibling(treeRoot(t)));
      size += treeSize(nodeRightSibling(treeRoot(t)));
    }
  }

  return size;
}
uint         treeHeight     (TreePtr t)
{
  uint height = 0;
  // Caso base: arbol vacio, no hacer nada (altura es 0)
  if (!treeEmpty(t))
  {
    // Caso base: el arbol es una hoja (altura es 1)
    if (nodeIsLeaf(treeRoot(t)))
    {
      height = 0;
    }
    else
    {
      // Recursion
      if (!treeEmpty(t))
      {
        height++;
        uint hLeft = treeHeight(nodeLeftSibling(treeRoot(t)));
        uint hRight = treeHeight(nodeRightSibling(treeRoot(t)));

        height += (hLeft > hRight) ? hLeft : hRight;
      }
    }
  }

  return height;
}
uint         treeLeafNodes  (TreePtr t)
{
  uint count = 0;

  if (not treeEmpty(t))
  {
    // Caso base
    if (nodeIsLeaf(treeRoot(t)))
    {
      count++;
    }
    // Recursion
    else
    {
      if (not treeEmpty(nodeLeftSibling(treeRoot(t))))
        count += treeLeafNodes(nodeLeftSibling(treeRoot(t)));
      if (not treeEmpty(nodeRightSibling(treeRoot(t))))
        count += treeLeafNodes(nodeRightSibling(treeRoot(t)));
    }
  }

  return count;
}
NodePtr      treeSearch     (TreePtr t, Element x)
{
  if (not treeEmpty(t))
  {
    // Casos base
    if (nodeKey(treeRoot(t)) == x)
    {
      return treeRoot(t);
    }
    else if (nodeKey(treeRoot(t)) != x && nodeIsLeaf(treeRoot(t)))
    {
      return nullptr;
    }
    // Recursion
    else
    {
      if (x < nodeKey(treeRoot(t)))
      {
        if (treeEmpty(nodeLeftSibling(treeRoot(t))))
        {
          return nullptr;
        }
        else
        {
          return treeSearch(nodeLeftSibling(treeRoot(t)), x);
        }
      }
      else
      {
        if (treeEmpty(nodeRightSibling(treeRoot(t))))
        {
          return nullptr;
        }
        else
        {
          return treeSearch(nodeRightSibling(treeRoot(t)), x);
        }
      }
    }
  }
  else
    return nullptr;
}
NodePtr      treeParent     (TreePtr t, Element x)
{
  if (not treeEmpty(t))
  {
    // Casos base
    if (nodeIsLeaf(treeRoot(t)))
    {
      return nullptr;
    }
    else if (esPadre(t, x))
    {
      return treeRoot(t);
    }
    // Recursion
    else
    {
      NodePtr res = nullptr;

      // Si x es menor que key actual
      if (x < nodeKey(treeRoot(t)) && nodeLeftSibling(treeRoot(t)))
      {
        res = treeParent(nodeLeftSibling(treeRoot(t)), x);
      }
      // si x es mayor que el actual y existe el camino derecho
      else if(nodeRightSibling(treeRoot(t)))
      {
        res = treeParent(nodeRightSibling(treeRoot(t)), x);
      }

      return res;
    }
  }
  else
    return nullptr;
}
NodePtr      treeParent     (TreePtr t, NodePtr p)
{
  if (not treeEmpty(t) && p != nullptr)
  {
    // Casos base
    if (nodeIsLeaf(treeRoot(t)))
    {
      return nullptr;
    }
    else if (treeRoot(nodeLeftSibling(treeRoot(t))) == p || treeRoot(nodeRightSibling(treeRoot(t))) == p)
    {
      return treeRoot(t);
    }
    // Recursion
    else
    {
      NodePtr res = nullptr;

      // Si tiene un hijo a la izquierda y el valor está ahí
      if (nodeLeftSibling(treeRoot(t)) && nodeKey(p) < nodeKey(treeRoot(t)))
      {
        res = treeParent(nodeLeftSibling(treeRoot(t)), p);
      }
      // Si tiene un hijo a la deracha y no ha encontrado nada a la izquierda
      if (res == nullptr && nodeRightSibling(treeRoot(t)) && nodeKey(p) > nodeKey(treeRoot(t)))
      {
        res = treeParent(nodeRightSibling(treeRoot(t)), p);
      }

      return res;
    }
  }
  else
    return nullptr;
}
NodePtr      treeMaximum    (TreePtr t)
{
  if (not treeEmpty(t))
  {
    NodePtr max = treeRoot(t);

    for (; treeRoot(nodeRightSibling(max)); max = treeRoot(nodeRightSibling(max)));

    return max;
  }
  else
    return nullptr;
}
std::string  treePreOrder   (TreePtr t, char c)
{
  std::string res;

  if (treeEmpty(t))
    return {};

  // Caso base
  if (nodeIsLeaf(treeRoot(t)))
  {
    // Poner solo si no es el comienzo de la cadena
    res += nodeKey(treeRoot(t));
    res += c;
  }
  // Recursiva
  else
  {
    res += nodeKey(treeRoot(t));
    res += c;
    if (not treeEmpty(nodeLeftSibling(treeRoot(t))))
      res += treePreOrder(nodeLeftSibling(treeRoot(t)), c);
    if (not treeEmpty(nodeRightSibling(treeRoot(t))))
      res += treePreOrder(nodeRightSibling(treeRoot(t)), c);
  }

  // Eliminar el primer c
  // res.erase(0, 1);

  return res;
}
std::string  treePostOrder  (TreePtr t, char c)
{
  std::string res;

  if (treeEmpty(t))
    return {};

  // Caso base
  if (nodeIsLeaf(treeRoot(t)))
  {
    res += nodeKey(treeRoot(t));
    res += c;
  }
  // Recursiva
  else
  {
    if (not treeEmpty(nodeLeftSibling(treeRoot(t))))
      res += treePostOrder(nodeLeftSibling(treeRoot(t)), c);
    if (not treeEmpty(nodeRightSibling(treeRoot(t))))
      res += treePostOrder(nodeRightSibling(treeRoot(t)), c);
    res += nodeKey(treeRoot(t));
    res += c;
  }

  return res;
}
std::string  treeInOrder    (TreePtr t, char c)
{
  std::string res;

  if (treeEmpty(t))
    return {};

  // Caso base
  if (nodeIsLeaf(treeRoot(t)))
  {
    res += nodeKey(treeRoot(t));
    res += c;
  }
  // Recursiva
  else
  {
    if (not treeEmpty(nodeLeftSibling(treeRoot(t))))
      res += treeInOrder(nodeLeftSibling(treeRoot(t)), c);
    res += nodeKey(treeRoot(t));
    res += c;
    if (not treeEmpty(nodeRightSibling(treeRoot(t))))
      res += treeInOrder(nodeRightSibling(treeRoot(t)), c);
  }

  return res;
}

std::string  treeByLevels   (TreePtr t, char c)
{
  if (treeEmpty(t))
    return {};
  std::queue<Element> q;

  // Añadir la raiz a la cola
  q.push(nodeKey(treeRoot(t)));

  // Ejecutar recursivamente
  levels_recur(t, q);

  // Crear cadena
  std::string res;
  while(not q.empty())
  {
    res += q.front();
    res += c;
    q.pop();
  }

  // res.pop_back();

  return res;
}

NodePtr      nodeCreate        (Element e)
{
  NodePtr n = new Node();
  nodeKey(n) = e;
  treeRoot(nodeLeftSibling(n)) = nullptr;
  treeRoot(nodeRightSibling(n)) = nullptr;

  return n;
}

TreePtr      nodeLeftSibling   (NodePtr p)
{
  if (p)
    return &p->lefts;
  else
    return nullptr;
}

TreePtr      nodeRightSibling  (NodePtr p)
{
  if (p)
    return &p->rights;
  else
    return nullptr;
}

Element&     nodeKey           (NodePtr p)
{
    return p->_key;
}

bool         nodeIsLeaf        (NodePtr p)
{
  if (p)
    return (treeEmpty(&p->lefts) && treeEmpty(&p->rights)) ? true : false;
  else
    return false;
}
