#include "tree.h"
#include "graph.h"
#include <iostream>

int main(int argc, char const *argv[]) {
  TreePtr t = treeCreate();
  treeInsert(t, '5');
  treeInsert(t, '2');
  treeInsert(t, '6');
  treeInsert(t, '1');
  treeInsert(t, '8');
  treeInsert(t, '7');
  treeInsert(t, '9');
  treeInsert(t, '3');

  std::cout << treeByLevels(t, '-') << std::endl;

  treeDestroy(t);
  t = nullptr;

  GraphPtr g = graphCreate(5);

  graphInsert(g, 0, 1, 10);
  graphInsert(g, 0, 3, 30);
  graphInsert(g, 0, 4, 100);
  graphInsert(g, 1, 2, 50);
  graphInsert(g, 2, 4, 10);
  graphInsert(g, 3, 4, 60);
  graphInsert(g, 3, 2, 20);

  GraphPtr h = graphCreate(6, false);

  graphInsert(h, 0, 1, 6);
  graphInsert(h, 0, 3, 5);
  graphInsert(h, 0, 2, 1);
  graphInsert(h, 1, 2, 5);
  graphInsert(h, 1, 4, 3);
  graphInsert(h, 2, 3, 5);
  graphInsert(h, 2, 4, 6);
  graphInsert(h, 2, 5, 4);
  graphInsert(h, 3, 5, 2);
  graphInsert(h, 4, 5, 6);

  auto v = dijkstra(g, 0);
  auto u = prim(h, 0);

  std::cout << graphToString(g) << std::endl;

  std::cout << graphToString(h) << std::endl;

  std::cout << "Dijkstra desde el nodo " << 0 << std::endl;
  for (size_t i = 0; i < g->nv; ++i)
  {
    std::cout << "Desde el nodo " << 0 << " hasta el nodo " << i << ":" << v.at(i) << std::endl;
  }

  std::cout << std::endl << "Prim desde el nodo 0" << std::endl;
  for (auto it : u)
  {
    std::cout << "(" << it.o << " -- " << it.e << ")" << std::endl;
  }

  graphDestroy(g);
  graphDestroy(h);

  // Grafo chungo
  std::cout << "Grafo chungo" << std::endl;

  g = graphCreate(5, NOTDIRECTED);

  graphInsert(g, 0, 1, 10);
  graphInsert(g, 1, 2, 5);
  graphInsert(g, 0, 2, 20);
  graphInsert(g, 3, 4, 10);

  v = dijkstra(g, 0);
  u = prim(g, 0);

  std::cout << "Dijkstra desde el nodo " << 0 << std::endl;
  for (size_t i = 0; i < g->nv; ++i)
  {
    std::cout << "Desde el nodo " << 0 << " hasta el nodo " << i << ":" << v.at(i) << std::endl;
  }

  std::cout << std::endl << "Prim desde el nodo 0" << std::endl;
  for (auto it : u)
  {
    std::cout << "(" << it.o << " -- " << it.e << ")" << std::endl;
  }

  return 0;
}
